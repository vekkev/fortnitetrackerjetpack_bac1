package com.example.fortnitetrackerjetpack.fields

enum class UserDataFields {
    player1,
    player2,
    username
}