package com.example.fortnitetrackerjetpack.service

import android.content.Context
import com.example.fortnitetrackerjetpack.dao.UserDataRoomsEntity
import com.example.fortnitetrackerjetpack.mapper.UserData
import com.github.kittinunf.fuel.core.ResponseResultOf

interface SearchActivityService {
    fun fetchUserData(userName: String, platform: String): ResponseResultOf<String>

    fun storeInDatabase(userData: UserDataRoomsEntity)

    fun checkIfDataExists(userData: UserDataRoomsEntity, playerName: String, context: Context): Boolean

    fun checkIfDataExistsWithUserData(userData: UserData, playerName: String, context: Context): Boolean
}