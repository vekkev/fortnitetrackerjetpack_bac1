package com.example.fortnitetrackerjetpack.service

import com.github.kittinunf.fuel.core.ResponseResultOf
import com.github.kittinunf.fuel.httpGet

class ChallengeActivityServiceImpl :
    ChallengeActivityService {

    override fun fetchUserData(): ResponseResultOf<String> {
        val URL = "https://api.fortnitetracker.com/v1/challenges/"
        return URL.httpGet().header(
            "TRN-Api-Key",
            "0775b641-8998-4046-8c8d-3cd61817d5d1"
        ).responseString()
    }


    }

