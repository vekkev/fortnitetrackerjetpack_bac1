package com.example.fortnitetrackerjetpack.service

import android.content.Context

import android.widget.Toast
import com.example.fortnitetrackerjetpack.dao.AppDatabase
import com.example.fortnitetrackerjetpack.dao.UserDataRoomsEntity
import com.example.fortnitetrackerjetpack.mapper.UserData
import com.github.kittinunf.fuel.core.ResponseResultOf
import com.github.kittinunf.fuel.httpGet

class SearchActivityServiceImpl(private val database: AppDatabase) :
    SearchActivityService {

    override fun fetchUserData(userName: String, platform: String): ResponseResultOf<String> {
        val URL = "https://api.fortnitetracker.com/v1/profile/${platform}/$userName"
        return URL.httpGet().header(
            "TRN-Api-Key",
            "0775b641-8998-4046-8c8d-3cd61817d5d1"
        ).responseString()
    }

    override fun storeInDatabase(userData: UserDataRoomsEntity) {
        database.userStatsRoomsDao().insertUser(userData)
    }

    override fun checkIfDataExists(userData: UserDataRoomsEntity, playerName: String, context: Context): Boolean {
        val message = "Could not find Data for "
        database.userStatsRoomsDao().getGivenUser(userData.accountid)
        if (userData.accountid.isNullOrEmpty()) {
            val errorMessage = Toast.makeText(context, message + playerName, Toast.LENGTH_LONG)
            errorMessage.show()
            return false
        }
        return true
    }

    override fun checkIfDataExistsWithUserData(userData: UserData, playerName: String, context: Context): Boolean {
        val message = "Could not find Data for "
        if (userData.accountId.isNullOrEmpty()) {
            val errorMessage = Toast.makeText(context, message + playerName, Toast.LENGTH_LONG)
            errorMessage.show()
            return false
        }
        return true
    }
}