package com.example.fortnitetrackerjetpack.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserStatsRoomsDao {

    @Query("SELECT * FROM userdataroomsentity WHERE epicUserHandle LIKE :username")
    fun getGivenUser(username : String) : UserDataRoomsEntity

    @Insert
    fun insertUser(data: UserDataRoomsEntity)
}