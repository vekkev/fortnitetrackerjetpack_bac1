package com.example.fortnitetrackerjetpack.dao

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserDataRoomsEntity(
    @PrimaryKey(autoGenerate = true)
    var id: Int,
    var accountid: String = "",
    var epicUserHandle: String = "",
    var platformNameLong: String = "",
    var top5: String = "",
    var top3: String = "",
    var top6: String = "",
    var top10: String = "",
    var top12: String = "",
    var top25: String = "",
    var score: String = "",
    var matchesPlayed: String = "",
    var wins: String = "",
    var winsPer: String = "",
    var kills: String = "",
    var kd: String = ""
) {
}