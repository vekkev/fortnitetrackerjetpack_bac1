package com.example.fortnitetrackerjetpack.activities

import android.os.Bundle
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.room.Room
import com.example.fortnitetracker.R
import com.example.fortnitetracker.databinding.ActivityStatisticBinding
import com.example.fortnitetrackerjetpack.activities.databinding.UserDataBinding
import com.example.fortnitetrackerjetpack.dao.AppDatabase
import com.example.fortnitetrackerjetpack.dao.UserDataRoomsEntity
import com.example.fortnitetrackerjetpack.fields.UserDataFields
import org.jetbrains.anko.find

class StatisticActivity : AppCompatActivity() {

    //needed for DataBinding
    private lateinit var binding: ActivityStatisticBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //for DataBinding
        binding = DataBindingUtil.setContentView(this, R.layout.activity_statistic)

        val intent = intent

        val username = intent.extras!!.getString(UserDataFields.username.name).toString()

        val database =
            Room.databaseBuilder(applicationContext, AppDatabase::class.java, "userstats.db")
                .allowMainThreadQueries().build()
        val userData = database.userStatsRoomsDao().getGivenUser(username)

        fillViewWithData(userData)
    }

    private fun fillViewWithData(userData: UserDataRoomsEntity) {

        var persInform = buildPersInfString(userData)
        var ranking = buildRankInfString(userData)
        var scores = buildScoresInfString(userData)
        var userDataBinding = UserDataBinding(persInform, ranking, scores)
        binding.userData = userDataBinding

    }

    private fun buildScoresInfString(userData: UserDataRoomsEntity) =
        "Score: " + userData.score + "\n" + "Matches Played: " + userData.matchesPlayed + "\n" + "Wins: " + userData.wins + "\n" +
                "Wins in %: " + userData.winsPer + "\n" + "Kills: " + userData.kills + "\n" + "K/D: " + userData.kd + "\n"

    private fun buildRankInfString(userData: UserDataRoomsEntity) =
        "Top 5: " + userData.top5 + "\n" + "Top 3: " + userData.top3 + "\n" + "Top 6: " + userData.top6 + "\n" + "Top 10: " +
                userData.top10 + "\n" + "Top 12: " + userData.top12 + "\n" + "Top 25: " + userData.top25 + "\n"

    private fun buildPersInfString(userData: UserDataRoomsEntity) =
        "Username: " + userData.epicUserHandle + "\n" + "Plattform: " + userData.platformNameLong + "\n"
}