package com.example.fortnitetrackerjetpack.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.room.Room
import com.example.fortnitetracker.R
import com.example.fortnitetrackerjetpack.dao.AppDatabase

class MainActivity : AppCompatActivity() {

    private lateinit var searchButton: Button
    private lateinit var comparisionButton: Button
    private lateinit var challengesButton: Button
    private val featureEnabler = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchButton = findViewById(R.id.buttonSearch)
        comparisionButton = findViewById(R.id.buttonCompare)

        challengesButton = findViewById(R.id.buttonChallenges)
        challengesButton.setOnClickListener(challengeClickListener())


        searchButton.setOnClickListener(searchClickListener())
        comparisionButton.setOnClickListener(comparisonClickListener())

        val db = Room.databaseBuilder(applicationContext, AppDatabase::class.java, "userstats.db")
            .allowMainThreadQueries().build()

    }

    private fun comparisonClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, CompareActivity::class.java)
            startActivity(intent)
        }
    }

    private fun searchClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }

    private fun challengeClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, ChallengeActivity::class.java)
            startActivity(intent)

        }

    }
}
