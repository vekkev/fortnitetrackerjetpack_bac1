package com.example.fortnitetrackerjetpack.activities

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.room.Room
import com.example.fortnitetracker.R
import com.example.fortnitetrackerjetpack.dao.AppDatabase
import com.example.fortnitetrackerjetpack.dao.UserDataRoomsEntity
import com.example.fortnitetrackerjetpack.fields.UserDataFields
import com.example.fortnitetrackerjetpack.mapper.UserData
import com.example.fortnitetrackerjetpack.service.SearchActivityService
import com.example.fortnitetrackerjetpack.service.SearchActivityServiceImpl
import com.google.gson.Gson
import java.lang.RuntimeException

class SearchActivity : AppCompatActivity(), View.OnClickListener {

    //initialisation of widgets
    private lateinit var buttonSearch: Button
    private lateinit var usernameEditText: EditText
    private lateinit var platform: Spinner
    private lateinit var searchActivityService: SearchActivityService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        //prevents app from deadlock
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        val db =
            Room.databaseBuilder(applicationContext, AppDatabase::class.java, "userstats.db")
                .allowMainThreadQueries()
                .build()

        //allocation of the widgets
        buttonSearch = findViewById(R.id.getStatsButton)
        usernameEditText = findViewById(R.id.accountName)
        platform = findViewById(R.id.platform)
        buttonSearch.setOnClickListener(this)
        searchActivityService =
            SearchActivityServiceImpl(db)
    }

    override fun onClick(v: View?) {

        val userName = usernameEditText.text.toString()
        try {
            val gson = Gson()
            val fetchUserData =
                searchActivityService.fetchUserData(userName, platform.selectedItem.toString())
            val userData = gson.fromJson(fetchUserData.third.component1(), UserData::class.java)

            val userDataRoomsEntity = UserDataRoomsEntity(
                0,
                userData.accountId,
                userData.epicUserHandle,
                userData.platformNameLong,
                userData.lifeTimeStats[0].value,
                userData.lifeTimeStats[1].value,
                userData.lifeTimeStats[2].value,
                userData.lifeTimeStats[3].value,
                userData.lifeTimeStats[4].value,
                userData.lifeTimeStats[5].value,
                userData.lifeTimeStats[6].value,
                userData.lifeTimeStats[7].value,
                userData.lifeTimeStats[8].value,
                userData.lifeTimeStats[9].value,
                userData.lifeTimeStats[10].value
            )

            searchActivityService.storeInDatabase(userDataRoomsEntity)
            val intent = Intent(this, StatisticActivity::class.java)
            intent.putExtra(UserDataFields.username.name, userName.toLowerCase())
            startActivity(intent)
        } catch (e: RuntimeException) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Service currently unavailable!", Toast.LENGTH_LONG).show()
        }


    }
}
