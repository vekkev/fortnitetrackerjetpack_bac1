package com.example.fortnitetrackerjetpack.activities

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R
import com.example.fortnitetrackerjetpack.dao.AppDatabase
import com.example.fortnitetrackerjetpack.fields.UserDataFields
import com.example.fortnitetrackerjetpack.mapper.UserData
import com.example.fortnitetrackerjetpack.service.SearchActivityService
import com.example.fortnitetrackerjetpack.service.SearchActivityServiceImpl
import com.github.kittinunf.fuel.core.ResponseResultOf
import com.google.gson.Gson
import java.lang.RuntimeException

class CompareActivity : AppCompatActivity() {

    private lateinit var searchActivityService: SearchActivityService
    private lateinit var searchButton: Button
    private lateinit var player1: EditText
    private lateinit var player2: EditText
    private lateinit var player1Platform: Spinner
    private lateinit var player2Platform: Spinner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compare)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()

        StrictMode.setThreadPolicy(policy)

        val userStatsDao = AppDatabase(this)
        searchActivityService =
            SearchActivityServiceImpl(userStatsDao)

        searchButton = findViewById(R.id.bnStartCompare)
        player1 = findViewById(R.id.etPlayer1)
        player2 = findViewById(R.id.etPlayer2)
        player1Platform = findViewById(R.id.platformComparePlayer1)
        player2Platform = findViewById(R.id.platformComparePlayer2)

        searchButton.setOnClickListener(compareOnClickListener())
    }

    private fun compareOnClickListener(): View.OnClickListener {
        return View.OnClickListener {
            prepareDataForView()
        }
    }

    private fun prepareDataForView() {


        val gson = Gson()
        val player1Text = player1.text.toString()
        val player2Text = player2.text.toString()

        val platformPlayer1 = player1Platform.selectedItem.toString()
        val platformPlayer2 = player2Platform.selectedItem.toString()

        try {
            val fetchPlayer1 = fetchPlayerData(player1Text, platformPlayer1)
            val fetchPlayer2 = fetchPlayerData(player2Text, platformPlayer2)
            val userDataPlayer1 = convertToEntity(gson, fetchPlayer1)
            val userDataPlayer2 = convertToEntity(gson, fetchPlayer2)

            if (checkIfDataExists(userDataPlayer1, player1Text) && checkIfDataExists(
                    userDataPlayer2,
                    player2Text
                )
            ) {
                val intent = Intent(this, CompareActivityResult::class.java)
                intent.putExtra(UserDataFields.player1.name, userDataPlayer1)
                intent.putExtra(UserDataFields.player2.name, userDataPlayer2)
                startActivity(intent)
            }
        } catch (e: RuntimeException) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Service currently unavailable!", Toast.LENGTH_LONG).show()
        }

    }

    private fun fetchPlayerData(
        playerText: String,
        platformPlayer: String
    ) = searchActivityService.fetchUserData(playerText, platformPlayer)

    private fun convertToEntity(
        gson: Gson,
        fetchPlayer: ResponseResultOf<String>
    ) = gson.fromJson(fetchPlayer.third.component1(), UserData::class.java)

    private fun checkIfDataExists(
        userDataPlayer: UserData,
        playerText: String
    ) = searchActivityService.checkIfDataExistsWithUserData(userDataPlayer, playerText, this)
}
