package com.example.fortnitetrackerjetpack.activities

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R
import com.example.fortnitetrackerjetpack.mapper.ChallengeData
import com.example.fortnitetrackerjetpack.service.ChallengeActivityService
import com.example.fortnitetrackerjetpack.service.ChallengeActivityServiceImpl
import com.google.gson.Gson

class ChallengeActivity : AppCompatActivity() {

    private lateinit var challenges: EditText
    private lateinit var challengeActivity: ChallengeActivityService


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_challenge)

        //prevents app from deadlock
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        challenges = findViewById(R.id.challenges)
        challengeActivity = ChallengeActivityServiceImpl()


        try {
            val fetchUserData = challengeActivity.fetchUserData()

            val gson = Gson()
            val dataChallenge =
                gson.fromJson(fetchUserData.third.component1(), ChallengeData::class.java)

            fillViewWithData(dataChallenge, challenges, dataChallenge.items.size)
        } catch (e: RuntimeException) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Service currently unavailable!", Toast.LENGTH_LONG).show()
        }

    }

    private fun fillViewWithData(challengeData: ChallengeData, challenges: EditText, size: Int) {

        for (x in 0 until size) {
            challenges.append("Typ: " + challengeData.items[x].metadata[0].value + "\n")
            challenges.append("Name der Challenge: \n" + challengeData.items[x].metadata[1].value + "\n")
            challenges.append("\n")
        }

    }


}