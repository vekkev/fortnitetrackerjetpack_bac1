package com.example.fortnitetrackerjetpack.mapper

import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LifeTimeStats(val key: String = "", val value: String = "") : Parcelable {
    class Deserializer : ResponseDeserializable<LifeTimeStats> {
        override fun deserialize(content: String): LifeTimeStats = Gson().fromJson(content, LifeTimeStats::class.java)
    }
}