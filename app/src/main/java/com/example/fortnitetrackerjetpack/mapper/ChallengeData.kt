package com.example.fortnitetrackerjetpack.mapper

import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ChallengeData (
    val items: List<Items>,
    val metadata: List<MetaData>
    )

    : Parcelable {

    class Deserializer : ResponseDeserializable<ChallengeData>{
        override fun deserialize(content: String): ChallengeData = Gson().fromJson(content, ChallengeData::class.java)
        }
    }


