package com.example.fortnitetrackerjetpack.mapper

import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MetaData(val key: String ="", val value: String ="" ) : Parcelable {
    class Deserializer : ResponseDeserializable<MetaData> {
        override fun deserialize(content: String): MetaData = Gson().fromJson(content, MetaData::class.java)
    }
}